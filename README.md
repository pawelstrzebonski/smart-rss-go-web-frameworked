# Smart-RSS Go Web Frameworked

A machine learning enhanced RSS feed manager (written in Go). Largely similar in intent to the parallel project [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web), this application uses front-end web frameworks for client-side rendering (rather than server-side templating/rendering in Smart-RSS Go Web). Both of these applications are built on the [`libsmartrssgo`](https://gitlab.com/pawelstrzebonski/libsmartrssgo) library.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/smart-rss-go-web-frameworked/).

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice

## Screenshots

TODO

## Usage/Installation

This repository contains the Go source code in the top level. It can be built using the standard Go compiler and may require installation of build dependencies from your systems package respositories. A `Makefile` is included with most of the relevant build/install commands, although build dependency installation is operating system dependent and is not included.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.

Once built, run the `smart-rss-go-web-frameworked` binary to start the back-end server. To access the UI, open in your browser of choice `localhost:8081` (this may be different depending on platform). The application takes command-line arguments `-port` (port number on which to serve this application), `-pure` (do not read/write to the database, use default template, primarily for debugging), and `-db $DATABASEFILENAME` to define the database to create/use by filename.
