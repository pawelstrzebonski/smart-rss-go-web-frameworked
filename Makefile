## ============== Smart-RSS Go Web Frameworked Makefile ===========
## ----------------------------------------------------
## This Makefile should support building using stardard
## Go toolset, as well as the Nix toolset.
## ----------------------------------------------------

help:			## Show this help
	@sed -ne '/@sed/!s/## //p' Makefile

build: go.sum		## Build application using Go toolset
	go build

install: smart-rss-go-web-frameworked	## Install application using Go toolset
	go install

smart-rss-go-web-frameworked: go.sum	## Build application using Go toolset
	go build

go.sum:			## Setup Go dependencies packages using Go toolset
	go mod tidy

build-nix:		## Build using Nix toolset
	nix-build

install-nix: result	## Install using Nix toolset
	nix-env -i ./result

result:			## Build using Nix toolset
	nix-build

build-docs:		## Build documentation pages
	mkdocs build

public:			## Build documentation pages
	mkdocs build

serve-docs: public	## Serve documentation pages (locally)
	mkdocs serve

clean:			## Clean up directory by deleting files
	rm -f smart-rss-go-web-frameworked
	rm -f result
	rm -f go.sum
	rm -rf public/
