module pawelstrzebonski/smart-rss-go-web-frameworked

go 1.19

require gitlab.com/pawelstrzebonski/libsmartrssgo v0.0.0-20230128190834-5d97525cd8a2

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/jbrukh/bayesian v0.0.0-20200318221351-d726b684ca4a // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/microcosm-cc/bluemonday v1.0.22 // indirect
	github.com/mmcdole/gofeed v1.1.3 // indirect
	github.com/mmcdole/goxpp v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/text v0.6.0 // indirect
)
