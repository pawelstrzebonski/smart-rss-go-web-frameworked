# Smart-RSS Go Web Frameworked

Smart-RSS Go Web Frameworked is a machine learning enhanced RSS feed manager, written in Go, with a back-end web-server and web-UI front-end. There is a parallel project, [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web) which provides much of the same functionality, but using server-side templating/rendering of HTML (no javascript). Both of these projects use [libsmartrssgo](https://gitlab.com/pawelstrzebonski/libsmartrssgo) as a backend for the RSS and machine learning functionality.

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice

## Screenshots

TODO
