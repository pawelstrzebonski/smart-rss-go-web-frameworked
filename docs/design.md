# Design Document

## Files

As of writing, there are three main files that comprise this application's source:

* `main.go`
* TODO

### `main.go`

This is the Go source file.

First, it will setup the database/RSS/machine-learning back-end provided by the `libsmartrssgo` package. It will load or create the database for RSS feeds/items, update the feeds on start, and setup an automatic periodic check for updates.

Next, it will setup the router/web-server. This will enable a user to access the main UI in their web browser of choice (likely at a URL of `localhost:8081`). The custom router will work with the server provided by `net/http` create and server the web UI pages and handle user feedback, passing them along to the `libsmartrssgo` backend, as appropriate.

### TODO
