# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Better UI:
	* expose more application settings
	* icons?
	* count # of unread items
	* nicer presentation
* Better handling of non-ASCII/international content
	* internationalization?
* General code/project improvements:
	* tests
	* CI
	* build documentation from source
