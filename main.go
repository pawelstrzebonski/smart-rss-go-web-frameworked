package main

import (
	"embed"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/pawelstrzebonski/libsmartrssgo"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// If an error, log as fatal
func logErrFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Embed assets into binary as a filesystem
//
//go:embed assets/*
var f embed.FS

// Load file from either real or embedded filesystem
func getfile(filename string, apppath string, ispure bool) ([]byte, error) {
	log.Print("Querying " + filename)
	// Check embedded filesystem for file
	embeddata, embederr := f.ReadFile(filename)
	// If in pure mode or no such file embedded, return as found
	/*
	 * Note: Always check if file is in embedded file system before
	 * querying local file system!
	 * We only want to allow serving of local versions of included files.
	 * Without this check, the server may be all too willing to serve
	 * user's files outside of what was intended!
	 */
	if embederr != nil || ispure {
		return embeddata, embederr
	} else {
		// Otherwise, first check if actual file is in user's filesystem
		fp := filepath.Join(apppath, filename)
		_, err := os.Stat(fp)
		if errors.Is(err, os.ErrNotExist) {
			// If not existant, then return from embedded filesystem
			return embeddata, embederr
		} else {
			// If existant, read from filesystem
			p, err := os.ReadFile(fp)
			return p, err
		}
	}
}

func main() {
	// Get execution options from flags
	pure := flag.Bool("pure", false, "Enable pure/in-memory mode (does not access files, will lose all data on restart)")
	port := flag.Int("port", 8081, "Port on which to server application")
	dbfile := flag.String("db", "smartrss_webframework.gob", "Name of database file (within configuration directory)")
	PASSWORD := flag.String("password", "", "Password for authentication")
	ishttps := flag.Bool("https", false, "Enable HTTPS mode (using program-generated self-signed certificate)")
	framework := flag.String("framework", "vue", "Which web framework to use")
	flag.Parse()
	// Setup the filepath for the database
	homepath, err := os.UserHomeDir()
	logErrFatal(err)
	apppath := filepath.Join(homepath, ".config", "smartrss")
	if !*pure {
		err = os.MkdirAll(apppath, os.ModePerm)
		logErrFatal(err)
	}
	// Create/load the database
	dbpath := filepath.Join(apppath, *dbfile)
	if *pure {
		// Create in-memory template in pure mode
		dbpath = ""
	}
	log.Print("Setting up back-end")
	smartrss := libsmartrssgo.Setup(dbpath, libsmartrssgo.ALL, true)
	// Defer closing/saving the database
	defer smartrss.Close()
	// Updated feeds on application start
	smartrss.FeedsUpdate()
	// Set automatic feed updating to run every 600 seconds
	smartrss.SetAutoUpdate(600)
	// Set automatic database saving run every 3600 seconds
	smartrss.SetAutoSave(3600)
	log.Print("Back-end set up")

	numitemsperpage := 4

	// Create own request router
	router := func(w http.ResponseWriter, r *http.Request) {
		// Get username (not checked) and password
		if len(*PASSWORD) > 0 {
			_, password, ok := r.BasicAuth()
			if !ok {
				w.Header().Add("WWW-Authenticate", `Basic realm="Give username and password"`)
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(`{"message": "No basic auth present"}`))
				return
			}
			// Compare password from browser to password from launch arguments
			if password != *PASSWORD {
				w.Header().Add("WWW-Authenticate", `Basic realm="Give username and password"`)
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(`{"message": "Invalid username or password"}`))
				return
			}
		}
		// Create appropriate response based on path and request method
		if r.Method == "GET" {
			switch r.URL.Path {
			case "/":
				{
					p, err := getfile("assets/"+*framework+"/index.html", apppath, *pure)
					logErrFatal(err)
					log.Print("Serving index.html")
					w.Write(p)
				}
			case "/favicon.ico":
				{
					p, err := getfile("assets/favicon.ico", apppath, *pure)
					logErrFatal(err)
					log.Print("Serving favicon")
					w.Write(p)
				}
			case "/numitemsperpage":
				{
					err = json.NewEncoder(w).Encode(struct{ Num int }{numitemsperpage})
					logErrFatal(err)
				}
			case "/items":
				{
					items := smartrss.ItemsUnreadGetLimited(uint64(numitemsperpage))
					log.Print("Serving up to ", numitemsperpage, " items, ", len(items), " in actuality.")
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					err = json.NewEncoder(w).Encode(items)
					logErrFatal(err)
				}
			case "/feeds":
				{
					feeds := smartrss.FeedsList()
					log.Print("Serving up feeds list.")
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(http.StatusOK)
					err = json.NewEncoder(w).Encode(feeds)
					logErrFatal(err)
				}
			default:
				{
					path := r.URL.Path
					p, err := getfile("assets/"+*framework+path, apppath, *pure)
					if err == nil {
						log.Print("Serving " + path)
						w.Write(p)
					} else {
						//Default on bad path
						http.Error(w, "404 not found.", http.StatusNotFound)
					}
				}
			}
		} else if r.Method == http.MethodPut {
			switch r.URL.Path {
			case "/add":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					url := r.PostFormValue("url")
					num, _ := strconv.Atoi(r.PostFormValue("num"))
					log.Print("Adding feed ", url, " with update period of ", num)
					smartrss.FeedAdd(url, uint64(num))
					w.WriteHeader(http.StatusOK)
				}
			case "/remove":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					url := r.PostFormValue("url")
					log.Print("Removing feed ", url)
					smartrss.FeedRemove(url)
					w.WriteHeader(http.StatusOK)
				}
			case "/like":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					url := r.PostFormValue("url")
					log.Print("Liking feed ", url)
					smartrss.ItemLike(url)
					w.WriteHeader(http.StatusOK)
				}
			case "/dislike":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					url := r.PostFormValue("url")
					log.Print("Disliking item ", url)
					smartrss.ItemDislike(url)
					w.WriteHeader(http.StatusOK)
				}
			case "/open":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					url := r.PostFormValue("url")
					log.Print("Opened item ", url)
					smartrss.ItemMarkOpened(url)
					w.WriteHeader(http.StatusOK)
				}
			case "/viewed":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					urls := r.PostFormValue("urls")
					log.Print("Marking viewed items ", urls)
					for _, url := range strings.Split(urls, ",") {
						smartrss.ItemMarkViewed(url)
					}
					w.WriteHeader(http.StatusOK)
				}
			case "/setitemsperpage":
				{
					if err := r.ParseMultipartForm(10_000); err != nil {
						fmt.Fprintf(w, "ParseForm() err: %v", err)
						return
					}
					num, _ := strconv.Atoi(r.PostFormValue("num"))
					numitemsperpage = num
					w.WriteHeader(http.StatusOK)
				}
			default:
				//Default on bad path
				http.Error(w, "404 not found.", http.StatusNotFound)
			}
		} else {
			http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
		}
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", router)
	// Launch the web server
	if *ishttps && !*pure {
		certfile, keyfile := filepath.Join(apppath, "cert.pem"), filepath.Join(apppath, "key.pem")
		// Check if certificate files already exist
		_, err = os.Stat(certfile)
		// If not, generate them
		if errors.Is(err, os.ErrNotExist) {
			keygen(fmt.Sprintf(":%d", *port), apppath)
		}
		http.ListenAndServeTLS(fmt.Sprintf(":%d", *port), certfile, keyfile, mux)
	} else {
		http.ListenAndServe(fmt.Sprintf(":%d", *port), mux)
	}
}
