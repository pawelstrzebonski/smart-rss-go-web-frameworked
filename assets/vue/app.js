const {
    createApp
} = Vue

createApp({
    data() {
        return {
            items: null,
            text: '',
            newfeedurl: "",
            newfeedupdatetime: 3600,
            numperpage: null,
            feeds: null,
            selectedfeed: "",
        }
    },
    methods: {
        getitems() {
            fetch('items', {
                    method: 'GET',
                })
                .then(response => {
                    response.json().then(res => this.items = res)
                })
                .catch(err => {
                    console.error(err);
                });
            console.error(this.items);
        },
        getnumperpage() {
            fetch('numitemsperpage', {
                    method: 'GET',
                })
                .then(response => {
                    response.json().then(res => this.numperpage = res['Num'])
                })
                .catch(err => {
                    console.error(err);
                });
            console.error(this.items);
        },
        setnumperpage() {
            const formData = new FormData();
            formData.append('num', this.numperpage);
            fetch('setitemsperpage', {
                    method: 'PUT',
                    body: formData,
                })
                .then(response => {
                    response.then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            this.getitems();
        },
        nextpage() {
            const formData = new FormData();
            formData.append('urls', this.items.map(i => i['Url']));
            fetch('viewed', {
                    method: 'PUT',
                    body: formData,
                })
                .then(response => {
                    response.then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            this.getitems();
        },
        updatefeedlist() {
            fetch('feeds', {
                    method: 'GET',
                })
                .then(response => {
                    response.json().then(res => this.feeds = res.map(r => r['Url']))
                })
                .catch(err => {
                    console.error(err);
                });
        },
        removefeed(e) {
            fetch('removefeed', {
                    method: 'DELETE',
                    headers: {
                        'url': e,
                    }
                })
                .then(response => {
                    response.json().then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            console.log(e);
            this.updatefeedlist()
        },
        addfeed(url, time) {
            const formData = new FormData();
            formData.append('url', url);
            formData.append('num', time);
            fetch('add', {
                    method: 'PUT',
                    body: formData,
                })
                .then(response => {
                    response.then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            console.log(url, time);
            this.updatefeedlist()
        },
        like(url) {
            const formData = new FormData();
            formData.append('url', url);
            fetch('like', {
                    method: 'PUT',
                    body: formData,
                })
                .then(response => {
                    response.then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            console.log(url)
        },
        dislike(url) {
            const formData = new FormData();
            formData.append('url', url);
            fetch('dislike', {
                    method: 'PUT',
                    body: formData,
                })
                .then(response => {
                    response.then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            console.log(url)
        },
        open(url) {
            window.open(url);
            const formData = new FormData();
            formData.append('url', url);
            fetch('open', {
                    method: 'PUT',
                    body: formData,
                })
                .then(response => {
                    response.then(res => console.log(res));
                })
                .catch(err => {
                    console.error(err);
                });
            console.log(url)
        },
    },
    beforeMount() {
        this.getnumperpage();
        this.getitems();
        this.updatefeedlist();
    }
}).mount('#app')
