{ pkgs ? import <nixpkgs> {} }:
with pkgs;

buildGoModule rec {
  name = "smartrssgowebframeworked";
  buildInputs = [
	go
  ];
  vendorSha256 = "sha256-ekJJhgZFbAvu3uf9SNwqYfeJe01CNZZ5stOopXj60sg=";
  rev = "f55ca04cea92d00b4a288d4efbff8b1a634d7b2b";
  src = fetchFromGitLab {
    inherit rev;
    owner = "pawelstrzebonski";
    repo = "smart-rss-go-web-frameworked";
    sha256 = "sha256-WT4cLRGTQNkKRKkrxAzFPabyrPn6leW6Ynrb2mzD3iM=";
  };
  meta = with lib; {
    description = "A machine learning enhanced RSS feed manager with a web-server back-end and a web-browser front-end, written in Go.";
    homepage = "https://gitlab.com/pawelstrzebonski/smart-rss-go-web-frameworked";
    license = licenses.agpl3;
    #maintainers = with maintainers; [ pawelstrzebonski ];
    platforms = platforms.linux;
  };
}
